import ddf.minim.*;
Minim minim;
AudioPlayer groove;
int degrees = 0;
int degrees2 = 0;
void setup() {
  fullScreen();
  strokeWeight(3);
  frameRate(3600);

  //--------------------------
  minim = new Minim(this);
  groove = minim.loadFile("Song2.mp3", 1030);
  groove.loop();
}
void draw() {
  background(0);
  stroke(round(random(255)), round(random(255)), round(random(255)));
  if ((frameCount/40)%4>2) {
    stroke(round(random(255)), round(random(255)), round(random(255)));
  }else if ((frameCount/40)%4<2){
    stroke(round(random(255)), round(random(255)), round(random(255)));
  }
  noFill();
  strokeWeight(10);
  ellipse(width/2, height/2, 100, 100);
  if (frameCount%1000>500) {
    Cirkel(200);
    degrees++;
    cirkel(400);
    degrees2--;
  }
  if (frameCount%1000<500) {
    Cirkel(200);
    degrees--;
    cirkel(400);
    degrees2++;
  }
  println(round(frameCount/40));
}

//----------------------------------------------------------------------------------
void Cirkel(int sizeCirkelBeat) {
  for (float i = 0; i<25; i++) {
    float degreesInRadians = radians(degrees);
    float x = (width/2+ cos(degreesInRadians-i) *sizeCirkelBeat);
    float y = (height/2+ sin(degreesInRadians-i) *sizeCirkelBeat);
    int beat = round(groove.left.level()*width); 
    if (beat < 0) {
      beat=0;
      ellipse(x, y, beat, beat);
    } else if (beat>500) {
      beat=beat/30;
      ellipse(x, y, beat, beat);
    } else {
      ellipse(x, y, beat, beat);
    }}}
  //-------------------------------------------------------------cirkel2
  void cirkel(int sizeCirkelbeat) {
     for (float i = 0; i<50; i++) {
  float degreesInRadians2 = radians(degrees2);
  float x2 = (width/2+ cos(degreesInRadians2-i) *sizeCirkelbeat);
  float y2 = (height/2+ sin(degreesInRadians2-i) *sizeCirkelbeat);
  int beat = round(groove.left.level()*width); 
  if (beat < 0) {
    beat=0;
    ellipse(x2, y2, beat, beat);
  } else if (beat>500) {
    beat=beat/30;
    ellipse(x2, y2, beat, beat);
  } else {
    ellipse(x2, y2, beat, beat);
  }}}