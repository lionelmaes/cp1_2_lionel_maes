void setup() {
  size(960, 540);
  background(0);
}

void draw() {
  if (keyCode==RIGHT) {
    fill(0, 0, 0, 10);
    noStroke();
    rect(0, 0, width, height);
    t4BeatEllipse();
  } else if (keyCode==UP) {
    BeatBezier();
  } else if (keyCode==LEFT) {
   fill(0, 0, 0, 10);
    noStroke();
    rect(0, 0, width, height);
    lijn();
  } else if (keyCode==DOWN) {
    fill(0, 0, 0, 10);
    noStroke();
    rect(0, 0, width, height);
    beatPaal();
  }
}

void t4BeatEllipse() {
  int hRand= round(random(10, 250));
  fill(0, 95, 255);
  ellipse(width/2, height/2, 350, 350);
  fill(255, 0, 255);
  ellipse(width/2, height/2, 50+hRand, 50+hRand);
}

void BeatBezier() {
  strokeWeight(5);
  noFill();
  stroke(random(255), random(255), random(255));
  bezier(0, height/2, random(0, 500), random(0, 500), random(0, 500), random(0, 500), width, height/2);
  fill(0, 0, 0, 10);
  noStroke();
  rect(0, 0, width, height);
}

int xPos=15;
void lijn() {
  background(0);
  fill(random(255), random(255), random(255));
  for (int i=0; i<40; i++) {
    int yRand=round(random(-150, 150));
    ellipse(xPos+i*25, height/2+yRand, 15, 15);
    if ((xPos+i*25)%width==0) {
      xPos=15;
    }
  }
  xPos++;
}

void beatPaal() {
  int xPos =width/2;
  int yRand =round(random(0, 500));
  int rKleur= round(random(0, 254));
  int gKleur= round(random(0, 254));
  int bKleur= round(random(0, 254));
  fill(rKleur, gKleur, bKleur);
  stroke(rKleur, gKleur, bKleur);
  line(0, height/2, xPos, yRand);
  line(xPos,yRand,width,height/2);
  ellipse(xPos, yRand, 20, 20);
  fill(0, 0, 0, 10);
  noStroke();
  rect(0, 0, width, height);
}