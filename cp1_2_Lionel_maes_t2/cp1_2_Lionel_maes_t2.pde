void setup() {
  size(500, 500);
  background(0);
  strokeWeight(5);
}

void draw() {
  int xPos =width/4*3;
  int yRand =round(random(0, 500));
  int rKleur= round(random(0,254));
  int gKleur= round(random(0,254));
  int bKleur= round(random(0,254));
  fill(rKleur,gKleur,bKleur);
  stroke(rKleur,gKleur,bKleur);
  line(0, height/2,xPos , yRand);
  ellipse(xPos, yRand, 20, 20);
  fill(0,0,0,10);
  noStroke();
  rect(0,0,width,height);
}